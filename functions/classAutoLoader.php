<?php
/**
 * Created by PhpStorm.
 * User: formation2
 * Date: 25/06/2018
 * Time: 16:19
 */
function classAutoLoader($className){
    require_once "./classes/" . $className . ".php";
}