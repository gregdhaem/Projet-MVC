<?php
/**
 * Created by PhpStorm.
 * User: formation2
 * Date: 25/06/2018
 * Time: 16:31
 */

abstract class Vivant implements Evolution
{
    use Metabolisme;

    public $nom;
    public $taxonomie;
    public $masse;

    public function mourir()
    {
        return "I am dead";
    }

    public function croitre($x)
    {
        // TODO: Implement Croitre() method.
        return "Je grandis de $x centimètres";
    }

    public function neAlive()
    {
        $this->respirer();
    }
}

